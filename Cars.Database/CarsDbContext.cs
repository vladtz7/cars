﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Cars.Database.Models;
using Cars.Database.Models.Identity;
using Cars.Database.Models.Types;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Cars.Database
{
    public class CarsDbContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public int LoggedUserId { get; set; }

        #region Identity Models

        public DbSet<UserRole> UserRoles { get; set; }

        #endregion Identity Models

        public DbSet<Car> Cars { get; set; }
        public DbSet<CarType> CarTypes { get; set; }

        public static CarsDbContext Create(IdentityFactoryOptions<CarsDbContext> options,
            IOwinContext context)
        {
            return new CarsDbContext(context.Get<DbRepositoryArgs>().ConnectionNameOrConnectionString,
                context.Get<DbRepositoryArgs>().LoggedUserId);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Identity Models

            User.AddModelDefinition(modelBuilder);
            Role.AddModelDefinition(modelBuilder);
            UserLogin.AddModelDefinition(modelBuilder);
            UserClaim.AddModelDefinition(modelBuilder);
            UserRole.AddModelDefinition(modelBuilder);

            #endregion Identity Models

            #region Types Models

            City.AddModelDefinition(modelBuilder);
            District.AddModelDefinition(modelBuilder);
            Country.AddModelDefinition(modelBuilder);

            #endregion Types Models

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        //TODO: use this for running migrations only!

        #region Constructors

        public CarsDbContext()
            : base("DefaultConnection")
        {
            RequireUniqueEmail = true;
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            LoggedUserId = 0;
        }

        public CarsDbContext(int? loggedUserId = null)
            : base("DefaultConnection")
        {
            RequireUniqueEmail = true;
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            LoggedUserId = loggedUserId ?? 0;
        }

        public CarsDbContext(string nameOrConnectionString, int? loggedUserID = null)
            : base(nameOrConnectionString)
        {
            RequireUniqueEmail = true;
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            LoggedUserId = loggedUserID ?? 0;
        }

        #endregion Constructor

        #region Types Models

        public DbSet<City> Cities { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Country> Countries { get; set; }

        #endregion Types Models

        #region Services

        #endregion Services
    }
}