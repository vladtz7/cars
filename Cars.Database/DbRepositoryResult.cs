﻿using System;

namespace Cars.Database
{
    public class DbRepositoryResult<T> : DbRepositoryResult
    {
        public T Payload { get; set; }

        public static DbRepositoryResult<T> Success(T payload)
        {
            return new DbRepositoryResult<T>
            {
                Message = string.Empty,
                Error = null,
                Status = DbRepositoryResultStatus.Success,
                Payload = payload
            };
        }

        public new static DbRepositoryResult<T> Failure(
            DbRepositoryResultStatus status = DbRepositoryResultStatus.Error, string message = null,
            Exception error = null)
        {
            var result = new DbRepositoryResult<T>
            {
                Status = status,
                Payload = default(T)
            };

            if (string.IsNullOrEmpty(message) == false)
                result.Message = message;

            if (error != null)
                result.Error = error;

            return result;
        }
    }

    public class DbRepositoryResult
    {
        private static readonly DbRepositoryResult SuccessfulResult;
        private static readonly DbRepositoryResult CreatedSuccessfulResult;
        private static readonly DbRepositoryResult UpdatedSuccessfulResult;
        private static readonly DbRepositoryResult DeletedSuccessfulResult;

        static DbRepositoryResult()
        {
            SuccessfulResult = new DbRepositoryResult
            {
                Message = string.Empty,
                Error = null,
                Status = DbRepositoryResultStatus.Success
            };

            CreatedSuccessfulResult = new DbRepositoryResult
            {
                Message = string.Empty,
                Error = null,
                Status = DbRepositoryResultStatus.Created
            };

            UpdatedSuccessfulResult = new DbRepositoryResult
            {
                Message = string.Empty,
                Error = null,
                Status = DbRepositoryResultStatus.Updated
            };

            DeletedSuccessfulResult = new DbRepositoryResult
            {
                Message = string.Empty,
                Error = null,
                Status = DbRepositoryResultStatus.Deleted
            };
        }

        public static DbRepositoryResult Success => SuccessfulResult;
        public static DbRepositoryResult CreatedSuccessful => CreatedSuccessfulResult;
        public static DbRepositoryResult UpdatedSuccessful => UpdatedSuccessfulResult;
        public static DbRepositoryResult DeletedSuccessful => DeletedSuccessfulResult;
        public DbRepositoryResultStatus Status { get; set; }
        public string Message { get; set; }
        public Exception Error { get; set; }

        public bool IsSuccess
            =>
                Status == DbRepositoryResultStatus.Success || Status == DbRepositoryResultStatus.Created ||
                Status == DbRepositoryResultStatus.Updated || Status == DbRepositoryResultStatus.Deleted;

        public string ErrorMessage
        {
            get
            {
                if (string.IsNullOrEmpty(Message) == false)
                    return Message;

                if (Error != null && Message != null && string.IsNullOrEmpty(Error.Message) == false)
                    return Error.Message;

                return "Unkown error";
            }
        }

        public static DbRepositoryResult Failure(DbRepositoryResultStatus status = DbRepositoryResultStatus.Error,
            string message = null, Exception error = null)
        {
            var result = new DbRepositoryResult
            {
                Status = status
            };

            if (string.IsNullOrEmpty(message) == false)
                result.Message = message;

            if (error != null)
                result.Error = error;

            return result;
        }
    }
}