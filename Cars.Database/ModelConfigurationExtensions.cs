﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq.Expressions;

namespace Cars.Database
{
    public static class ModelConfigurationExtensions
    {
        public static PrimitivePropertyConfiguration IdentityColumn<TStructuralType, T>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, T>> propertyExpression, string columnName, int? columnOrder = null)
            where T : struct
            where TStructuralType : class
        {
            var result = model.Property(propertyExpression)
                        .HasColumnName(columnName)
                        .IsRequired()
                        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static PrimitivePropertyConfiguration Column<TStructuralType, T>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, T>> propertyExpression, string columnName, int? columnOrder = null, bool isRequired = true)
            where T : struct
            where TStructuralType : class
        {
            var result = isRequired
                ? model.Property(propertyExpression).HasColumnName(columnName).IsRequired()
                : model.Property(propertyExpression).HasColumnName(columnName).IsOptional();

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static PrimitivePropertyConfiguration Column<TStructuralType, T>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, T?>> propertyExpression, string columnName, int? columnOrder = null)
            where T : struct
            where TStructuralType : class
        {
            var result = model.Property(propertyExpression).HasColumnName(columnName).IsOptional();

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static PrimitivePropertyConfiguration ImageColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, byte[]>> propertyExpression, string columnName, int? columnOrder = null, bool isRequired = true)
            where TStructuralType : class
        {
            var result = isRequired
                ? model.Property(propertyExpression).HasColumnName(columnName).HasColumnType("bytea").IsRequired()
                : model.Property(propertyExpression).HasColumnName(columnName).HasColumnType("bytea").IsOptional();

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static PrimitivePropertyConfiguration DateTimeColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, DateTime?>> propertyExpression, string columnName, int? columnOrder = null)
            where TStructuralType : class
        {
            var result = model.Property(propertyExpression).HasColumnName(columnName).IsOptional();

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static PrimitivePropertyConfiguration DateTimeColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, DateTime>> propertyExpression, string columnName, int? columnOrder = null, bool isRequired = true)
            where TStructuralType : class
        {
            var result = isRequired
                ? model.Property(propertyExpression).HasColumnName(columnName).IsRequired()
                : model.Property(propertyExpression).HasColumnName(columnName).IsOptional();

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static StringPropertyConfiguration StringColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, string>> propertyExpression, string columnName, int? columnOrder = null, bool isRequired = true, int stringMaxLength = DbConstants.StringLength)
            where TStructuralType : class
        {
            var result = isRequired
                ? model.Property(propertyExpression).HasColumnName(columnName).IsRequired().HasMaxLength(stringMaxLength)
                : model.Property(propertyExpression).HasColumnName(columnName).IsOptional().HasMaxLength(stringMaxLength);

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static StringPropertyConfiguration MaxStringColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, string>> propertyExpression, string columnName, int? columnOrder = null, bool isRequired = true)
            where TStructuralType : class
        {
            var result = isRequired
                ? model.Property(propertyExpression).HasColumnName(columnName).IsRequired().HasColumnType("text")
                : model.Property(propertyExpression).HasColumnName(columnName).IsOptional().HasColumnType("text");

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static DecimalPropertyConfiguration DecimalColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, decimal>> propertyExpression, string columnName, int? columnOrder = null, bool isRequired = true, byte precision = DbConstants.DecimalPrecision, byte scale = DbConstants.DecimalScale)
            where TStructuralType : class
        {
            var result = isRequired
                ? model.Property(propertyExpression).HasColumnName(columnName).IsRequired().HasPrecision(precision, scale)
                : model.Property(propertyExpression).HasColumnName(columnName).IsOptional().HasPrecision(precision, scale);

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static DecimalPropertyConfiguration DecimalColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, decimal?>> propertyExpression, string columnName, int? columnOrder = null, byte precision = DbConstants.DecimalPrecision, byte scale = DbConstants.DecimalScale)
            where TStructuralType : class
        {
            var result = model.Property(propertyExpression).HasColumnName(columnName).IsOptional().HasPrecision(precision, scale);

            if (columnOrder.HasValue)
                result = result.HasColumnOrder(columnOrder.Value);

            return result;
        }

        public static DecimalPropertyConfiguration GeographyDecimalColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, decimal>> propertyExpression, string columnName, int? columnOrder = null, bool isRequired = true, byte precision = DbConstants.GeographyDecimalPrecision, byte scale = DbConstants.GeographyDecimalScale)
            where TStructuralType : class
        {
            return model.DecimalColumn(propertyExpression, columnName, columnOrder, isRequired, precision, scale);
        }

        public static DecimalPropertyConfiguration GeographyDecimalColumn<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> model,
            Expression<Func<TStructuralType, decimal?>> propertyExpression, string columnName, int? columnOrder = null, byte precision = DbConstants.GeographyDecimalPrecision, byte scale = DbConstants.GeographyDecimalScale)
            where TStructuralType : class
        {
            return model.DecimalColumn(propertyExpression, columnName, columnOrder, precision, scale);
        }

        public static string Truncate(this string source, int length)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            if (source.Length > length)
                source = source.Substring(0, length);

            return source;
        }
    }
}
