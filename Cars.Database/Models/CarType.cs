﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cars.Database.Models.Identity;
using Cars.Database.Models.Types;

namespace Cars.Database.Models
{
    public class CarType : BaseModel
    {
        // I don't really know what informations this property should hold
        public string Name { get; set; }

        public virtual Country Country { get; set; }
        public virtual District District { get; set; }
        public virtual City City { get; set; }

        public virtual User AddedBy { get; set; }
        public virtual User ModifiedBy { get; set; }


        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var index = 0;
            var model = AddModelDefinition<Car>(modelBuilder, "cars", "public");

            model.Property(x => x.MakeName)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("makeName")
                .HasMaxLength(DbConstants.StringLength)
                .IsRequired();

            model.Property(x => x.ModelName)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("modelName")
                .HasMaxLength(DbConstants.StringLength)
                .IsRequired();

            model.Property(x => x.RegistrationDate)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("registrationDate")
                .IsRequired();

            model.Property(x => x.BodyColour)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("phone_no")
                .HasMaxLength(DbConstants.StringLength)
                .IsOptional();

            model.Property(x => x.NumberOfDoors)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("date_of_birth")
                .IsOptional();

            model.Property(x => x.Comments)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("comments")
                .HasMaxLength(DbConstants.BigStringLength)
                .IsOptional();
        }
    }
}
