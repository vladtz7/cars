﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cars.Database.Models
{
    public class BaseModel
    {
        protected const int IndexOfLastColumn = 5;

        protected BaseModel()
        {
            this.AddedAt = DateTime.UtcNow;
            this.IsDeleted = false;
        }

        public int Id { get; set; }
        public int AddedById { get; set; }
        public DateTime AddedAt { get; set; }
        public int? ModifiedById { get; set; }
        public DateTime? ModifiedAt { get; set; }
        [DisplayName("Status")]
        public bool IsDeleted { get; set; }

        protected static EntityTypeConfiguration<T> AddModelDefinition<T>(DbModelBuilder modelBuilder, string tableName, string schemaName = "public") where T : BaseModel
        {
            var model = modelBuilder.Entity<T>();

            model.ToTable(tableName, schemaName);
            model.HasKey(x => x.Id);
            model.IdentityColumn(x => x.Id, "id", 0);
            model.Column(x => x.AddedById, "addedById", 1);
            model.DateTimeColumn(x => x.AddedAt, "addedAt", 2);
            model.Column(x => x.ModifiedById, "modifiedById", 3);
            model.DateTimeColumn(x => x.ModifiedAt, "modifiedAt", 4);
            model.Column(x => x.IsDeleted, "isDeleted", 5);

            return model;
        }
    }
}
