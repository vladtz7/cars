﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Cars.Database.Models.Identity;

namespace Cars.Database.Models.Types
{
    public class City : BaseTypeModel
    {
        public City()
        {
            IsCapital = false;
        }

        public City(int id, string name, bool isCapital, int districtId, int visibilityIndex, bool isDeleted) : this()
        {
            Id = id;
            Name = name;
            IsCapital = isCapital;
            DistrictId = districtId;
            VisibilityIndex = visibilityIndex;
            IsDeleted = isDeleted;
            AddedAt = DateTime.UtcNow;
        }
        public bool IsCapital { get; set; }
        public int DistrictId { get; set; }
        public virtual District District { get; set; }
        public virtual User AddedBy { get; set; }
        public virtual User ModifiedBy { get; set; }

        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var index = 0;
            var model = AddModelDefinition<City>(modelBuilder, "cities", "types", false);

            model.Column(x => x.IsCapital, "isCapital", IndexOfLastColumn + ++index);
            model.Property(x => x.Name).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("cities_name_district_id_idx")
                {
                    IsUnique = true,
                    Order = 2
                }));
        }
    }
}
