﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Cars.Database.Models.Types
{
    public class BaseTypeModel : BaseModel 
    {
        protected new const int IndexOfLastColumn = 7;

        protected BaseTypeModel()
        {
            this.VisibilityIndex = 0;
            this.IsDeleted = false;
        }

        protected BaseTypeModel(string name, int addedById, int visibilityIndex = 0) : this()
        {
            this.Name = name;
            this.AddedById = addedById;
            this.VisibilityIndex = visibilityIndex;
        }

        public string Name { get; set; }
        public int VisibilityIndex { get; set; }

        protected static EntityTypeConfiguration<T> AddModelDefinition<T>(DbModelBuilder modelBuilder, string tableName, string schemaName = "public", bool isNameUnique = true) where T : BaseTypeModel
        {
            var model = BaseModel.AddModelDefinition<T>(modelBuilder, tableName, schemaName);

            model.StringColumn(x => x.Name, "name", BaseModel.IndexOfLastColumn + 1);
            model.Column(x => x.VisibilityIndex, "visibilityIndex", BaseModel.IndexOfLastColumn + 2);

            if (isNameUnique)
            {
                model.Property(x => x.Name).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute(tableName + "_name_idx") { IsUnique = true }));
            }
            return model;
        }
    }
}
