﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Cars.Database.Models.Identity;

namespace Cars.Database.Models.Types
{
    public class District : BaseTypeModel
    {

        public District()
        {
            Cities = new HashSet<City>();
        }

        public District(int id, string name, string code, int countryId, int visibilityIndex, bool isDeleted) : this()
        {
            Id = id;
            Name = name;
            Code = code;
            CountryId = countryId;
            VisibilityIndex = visibilityIndex;
            IsDeleted = isDeleted;
            AddedAt = DateTime.UtcNow;
            AddedById = 1;
        }

        public string Code { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public virtual User AddedBy { get; set; }
        public virtual User ModifiedBy { get; set; }

        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var model = AddModelDefinition<District>(modelBuilder, "districts", "types", false);

            model.StringColumn(x => x.Code, "code", IndexOfLastColumn + 1);
            model.Column(x => x.CountryId, "countryId", IndexOfLastColumn + 2);

            model.Property(x => x.CountryId)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("districts_name_country_id_idx")
                {
                    IsUnique = true,
                    Order = 1
                }));
            model.Property(x => x.Name)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("districts_name_country_id_idx")
                {
                    IsUnique = true,
                    Order = 2
                }));

            model.HasMany(x => x.Cities).WithRequired(x => x.District);
        }
    }
}
