﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Cars.Database.Models.Identity;

namespace Cars.Database.Models.Types
{
    public class Country : BaseTypeModel
    {
        public Country()
        {
        }

        public Country(int id, string name, string code, int visibilityIndex, bool isDeleted)
        {
            Id = id;
            Name = name;
            Code = code;
            VisibilityIndex = visibilityIndex;
            IsDeleted = isDeleted;
            AddedAt = DateTime.UtcNow;
            AddedById = 1;
        }

        public virtual ICollection<District> Districts { get; set; }
        public string Code { get; set; }
        public virtual User AddedBy { get; set; }
        public virtual User ModifiedBy { get; set; }

        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var model = AddModelDefinition<Country>(modelBuilder, "countries", "types", false);
            model.StringColumn(x => x.Code, "code", IndexOfLastColumn + 1);
            model.Property(x => x.Code).HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("countries_code_idx") {IsUnique = true}));
        }
    }
}