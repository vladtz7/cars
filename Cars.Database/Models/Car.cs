﻿using System.Data.Entity;
using Cars.Database.Models.Identity;

namespace Cars.Database.Models
{
    public class Car : BaseModel
    {
        // I don't really know what informations this property should hold
        public string MakeName { get; set; }
        public string ModelName { get; set; }
        public string RegistrationDate { get; set; }
        public string BodyColour { get; set; }
        public int NumberOfDoors { get; set; }
        public decimal EngineCapacity { get; set; }
        public string Comments { get; set; }

        public virtual User AddedBy { get; set; }
        public virtual User ModifiedBy { get; set; }


        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var index = 0;
            var model = AddModelDefinition<Car>(modelBuilder, "cars", "public");

            model.Property(x => x.MakeName)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("makeName")
                .HasMaxLength(DbConstants.StringLength)
                .IsRequired();

            model.Property(x => x.ModelName)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("modelName")
                .HasMaxLength(DbConstants.StringLength)
                .IsRequired();

            model.Property(x => x.RegistrationDate)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("registrationDate")
                .IsRequired();

            model.Property(x => x.BodyColour)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("phone_no")
                .HasMaxLength(DbConstants.StringLength)
                .IsOptional();

            model.Property(x => x.NumberOfDoors)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("date_of_birth")
                .IsOptional();
           
            model.Property(x => x.Comments)
                .HasColumnOrder(IndexOfLastColumn + index++)
                .HasColumnName("comments")
                .HasMaxLength(DbConstants.BigStringLength)
                .IsOptional();
        }
    }
}
