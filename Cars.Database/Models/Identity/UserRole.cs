﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cars.Database.Models.Identity
{
    public class UserRole : IdentityUserRole<int>
    {
        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<UserRole>();
            model.ToTable("userRoles", "identity");
            model.HasKey(x => new { x.UserId, x.RoleId});
            model.Column(x => x.UserId, "userId", 0);
            model.Column(x => x.RoleId, "roleId", 1);
        }
    }
}