using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cars.Database.Models.Identity
{
    public class UserClaim : IdentityUserClaim<int>
    {
        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<UserClaim>();
            model.ToTable("userClaims", "identity");

            model.IdentityColumn(x => x.Id, "id", 0);
            model.Column(x => x.UserId, "userId", 1);
            model.StringColumn(x => x.ClaimType, "claimType", 2);
            model.StringColumn(x => x.ClaimValue, "claimValue", 3, true, DbConstants.BigStringLength);

            model.Property(x => x.UserId).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("user_claims_user_id_claim_idx")
            {
                IsUnique = true,
                Order = 1
            }));
            model.Property(x => x.ClaimType).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("user_claims_user_id_claim_idx")
            {
                IsUnique = true,
                Order = 2
            }));
        }
    }
}