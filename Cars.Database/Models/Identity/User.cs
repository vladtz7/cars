using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cars.Database.Models.Identity
{
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {

        public User()
        {
            Uniquecode = Guid.NewGuid().ToString("N");

            FullName = string.IsNullOrEmpty(FullName) ? $"{FirstName} {LastName}" : FullName;

            IsDisabled = false;
            AddedAt = DateTime.UtcNow;
        }

        public int? AddedById { get; set; }
        public virtual User AddedBy { get; set; }
        public DateTime AddedAt { get; set; }
        public int? ModifiedById { get; set; }
        public virtual User ModifiedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string Password { get; set; }

        #region Identity Extension

        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string LastName { get; set; }
        public bool IsDisabled { get; set; }
        public string Uniquecode { get; set; }

        #endregion Identity Extension

        public virtual ICollection<User> UsersAdded { get; set; }
        public virtual ICollection<User> UsersModified { get; set; }
        
        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var index = 0;
            var model = modelBuilder.Entity<User>();
            model.ToTable("users", "identity");

            model.IdentityColumn(x => x.Id, "id", index++);
            model.StringColumn(x => x.FirstName, "firstName", index++, false);
            model.StringColumn(x => x.LastName, "lastName", index++, false);
            model.StringColumn(x => x.FullName, "fullName", index++, false);

            model.StringColumn(x => x.UserName, "userName", index++);
            model.StringColumn(x => x.PasswordHash, "psswordHash", index++);
            model.StringColumn(x => x.Password, "password", index++, false);
            model.StringColumn(x => x.Email, "email", index++);

            model.StringColumn(x => x.PhoneNumber, "phoneNo", index++, false);
            model.Column(x => x.AccessFailedCount, "noOfFailedLogins", index++);
            model.Column(x => x.IsDisabled, "isDisabled", index++);
            model.Column(x => x.AddedById, "addedById", index++);
            model.DateTimeColumn(x => x.AddedAt, "addedAt", index++);
            model.Column(x => x.ModifiedById, "modifiedById", index++);
            model.DateTimeColumn(x => x.ModifiedAt, "modifiedAt", index++);

            model.StringColumn(x => x.Uniquecode, "uniqueCode", index++);
            model.Property(x => x.Uniquecode).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("user_uniquecode_idx")
            {
                IsUnique = true
            }));

            model.Property(x => x.Email).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("users_email_idx")
            {
                IsUnique = true
            }));
            model.Property(x => x.UserName).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("users_username_idx")
            {
                IsUnique = true
            }));

            

            model.HasMany(x => x.UsersAdded).WithOptional(x => x.AddedBy);
            model.HasMany(x => x.UsersModified).WithOptional(x => x.ModifiedBy);
        }
    }
}