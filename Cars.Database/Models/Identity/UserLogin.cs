using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cars.Database.Models.Identity
{
    public class UserLogin : IdentityUserLogin<int>
    {
        public int Id { get; set; }

        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<UserLogin>();
            model.ToTable("userLogin", "identity");

            model.IdentityColumn(x => x.Id, "id", 0);
            model.Column(x => x.UserId, "userId", 1);
            model.StringColumn(x => x.LoginProvider, "loginProvider", 2);
            model.StringColumn(x => x.ProviderKey, "providerKey", 3, true, DbConstants.BigStringLength);
            
            model.Property(x => x.UserId).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("user_logins_user_id_claim_idx") { IsUnique = true, Order = 1 }));
            model.Property(x => x.LoginProvider).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("user_logins_user_id_claim_idx") { IsUnique = true, Order = 2 }));
        }
    }
}