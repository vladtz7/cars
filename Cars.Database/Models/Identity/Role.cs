﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cars.Database.Models.Identity
{
    public class Role : IdentityRole<int, UserRole>
    {
        public static void AddModelDefinition(DbModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Role>();
            model.ToTable("roles", "identity");

            model.IdentityColumn(x => x.Id, "id", 0);
            model.StringColumn(x => x.Name, "name", 1);
            
            model.Property(x => x.Name).HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("roles_name_idx")
            {
                IsUnique = true
            }));
        }
    }
}