﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Cars.Database
{
    public static class DbConstants
    {
        public const int StringLength20 = 20;
        public const int StringLength128 = 128;
        public const int StringLength = 256;
        public const int StringLength512 = 512;
        public const int StringLength1024 = 1024;
        public const int BigStringLength = 2048;
        public const int StringLength4096 = 4096;
        public const int StringLength8192 = 8192;
        public const byte DecimalPrecision = 18;
        public const byte DecimalScale = 4;
        public const byte GeographyDecimalPrecision = 18;
        public const byte GeographyDecimalScale = 15;

        public const string Anonymous = "Anonymous";

        public static class DefaultAdministratorUser
        {
            public const string FirstName = "Vlad";
            public const string LastName = "Dinu";
            public const string Email = "vladtz7@gmail.com";
            public const string PhoneNo = "0731760061";
            public const string Password = "vonino7";
        }

        public enum PublishStatus
        {
            [Description("Draft")] Draft = 0,
            [Description("Requires Approval")] RequiresApproval = 1,
            [Description("Pending")] Pending = 2,
            [Description("Published")] Published = 3
        }

        public enum WebPageType
        {
            [Description("Simple")] Simple = 0,
            [Description("Contact")] Contact = 1,
            [Description("Location")] Location = 2
        }

        public enum MenuItemType
        {
            [Description("Page")] Page = 0,
            [Description("Custom Link")] CustomLink = 1,
            [Description("EventsList")] EventsList = 2
        }

        public class MetaDataItem
        {
            public string TagName { get; set; }
            public string TagContent { get; set; }
            public Dictionary<string, string> Attributes { get; set; }
        }
        public class Role
        {
            public long Id { get; set; }
            public string Name { get; set; }
        }

        public static class RoleNames
        {
            public const string Administratives = Administrator + "," + PowerUser;
            public const string Administrator = "Administrator";
            public const string PowerUser = "PowerUser";
            public const string User = "User";
            public const string Client = "Client";
        }

        public static class Roles
        {
            public static Role Administrator => new Role { Id = 1, Name = RoleNames.Administrator };
            public static Role PowerUser => new Role { Id = 2, Name = RoleNames.PowerUser };
            public static Role User => new Role { Id = 3, Name = RoleNames.User };
            public static Role Client => new Role { Id = 4, Name = RoleNames.Client };

            public static Role[] All => new[] { Administrator, PowerUser, User, Client };
        }
    }
}
