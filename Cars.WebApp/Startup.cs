﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cars.WebApp.Startup))]
namespace Cars.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
